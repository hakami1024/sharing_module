package map.work;

import java.util.ArrayList;
import java.util.StringTokenizer;

import sports.Sport;
import android.app.Activity;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class SportMarker extends Activity
{

	private static final double LOCATION_EPS = 0.000001;

	private MarkerOptions marker;

	private double latitude;
	private double longitude;

	private int peopleCount = 1;

	private String master;

	private ArrayList<String> participants;

	private Sport sport;

	public SportMarker(double latitude, double longitude, String master,
			Sport sport)
	{
		this.latitude = latitude;
		this.longitude = longitude;
		this.master = master;
		this.sport = sport;
		peopleCount = 1;

		participants = new ArrayList<String>();

		marker = new MarkerOptions()
				.position( new LatLng( getLatitude(), getLongitude() ) )
				.title( sport.toString() )
				.icon( sport.getSportType().getMarkerIcon() );
		// .draggable(true);
	}

	public SportMarker()
	{
		this.latitude = 0;
		this.longitude = 0;

		this.master = null;
		this.sport = null;

		peopleCount = 1;

		marker = null;
	}

	public boolean isEqual( LatLng location )
	{
		return ( Math.abs( this.getLatitude() - location.latitude ) < LOCATION_EPS && Math
				.abs( this.getLongitude() - location.longitude ) < LOCATION_EPS );
	}

	@Override
	public String toString()
	{
		// TODO creator to Resource
		return sport.sportToString() + "\nCreator: " + master + "\nCount: "
				+ getPeopleCount();
	}

	public void addParticipant( String participant )
	{
		participants.add( participant );
	}

	public String participantsToString()
	{
		String res = "";
		if( participants.size() == 0 )
			return res;
		for( String part : participants )
			res += part + " ";
		return res.substring( 0, res.length() - 1 );
	}

	public void setparticipants( String participants )
	{
		this.participants.clear();
		StringTokenizer tokenizer = new StringTokenizer( " " );
		while( tokenizer.hasMoreTokens() )
		{
			this.participants.add( tokenizer.nextToken() );
		}
	}

	public MarkerOptions getMarker()
	{
		return marker;
	}

	public void setMarker( MarkerOptions marker )
	{
		this.marker = marker;
	}

	public double getLatitude()
	{
		return latitude;
	}

	public void setLatitude( double latitude )
	{
		this.latitude = latitude;
	}

	public double getLongitude()
	{
		return longitude;
	}

	public void setLongitude( double longitude )
	{
		this.longitude = longitude;
	}

	public int getPeopleCount()
	{
		return peopleCount;
	}

	public void setPeopleCount( int peopleCount )
	{
		this.peopleCount = peopleCount;
	}

	public void addOnePeople()
	{
		peopleCount++;
	}

	public void deleteOnePeople()
	{
		peopleCount--;
	}

	public String getMaster()
	{
		return master;
	}

	public void setMaster( String master )
	{
		this.master = master;
	}

	public Sport getSport()
	{
		return sport;
	}

	public void setSport( Sport sport )
	{
		this.sport = sport;
	}

}
