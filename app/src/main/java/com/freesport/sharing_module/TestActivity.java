package com.freesport.sharing_module;
import map.work.SportMarker;
import networking.Message;
import networking.Network;
import networking.Networking;
import sports.Sport;
import sports.SportTypes;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;
//TODO: Deal with this:
import com.example.hakami1024.myapplication.R;

public class TestActivity extends Activity
{
	private static final String TAG = "TestActivity";
	
	String info = "Хотите встретить создателей GoSport? Вам сюда.";
	double lat = 55.790256;
	double lon = 49.121755;

	@Override
	protected void onCreate( Bundle savedInstanceState )
	{
		super.onCreate( savedInstanceState );
		setContentView(R.layout.test_activity);
		Log.d( TAG, "onCreate started" );
		final SportMarker sportMarker = new SportMarker( lat, lon, "Master",
				new Sport( SportTypes.Other, "undefined", "undefined", info ) );
		final Message message = new Message( sportMarker );
		
		//String userName = Network.setNetwork( Network.VK, this ).InitUser().getUserName();
		//String userId = Network.getCurrentNetwork().InitUser().getUserId();
				
		Button btn_share_vk_wall = (Button)findViewById(R.id.btn_share_vk_wall);
		btn_share_vk_wall.setOnClickListener( new OnClickListener()
		{

			@Override
			public void onClick( View v )
			{
				Network
				.setNetwork( Network.VK, TestActivity.this )
				.sendMessage( Networking.WALL, message );				
			}
		} );
	
		Button btn_share_vk_private = (Button)findViewById(R.id.btn_share_vk_private);
		btn_share_vk_private.setOnClickListener( new OnClickListener()
		{
			@Override
			public void onClick( View v )
			{
				Network.setNetwork( Network.VK, TestActivity.this )
				.sendMessage( Networking.PRIVATE_MESSAGE, message );
			}
		} );
		
		
		Button btn_share_fb_wall = (Button)findViewById(R.id.btn_share_fb_wall);
		btn_share_fb_wall.setOnClickListener( new OnClickListener(){

			@Override
			public void onClick( View v )
			{
				Network.setNetwork( Network.FACEBOOK, TestActivity.this )
				.sendMessage( Networking.WALL, message );				
			}} );
	
		Button btn_share_fb_private = (Button)findViewById(R.id.btn_share_fb_private);
		btn_share_fb_private.setOnClickListener( new OnClickListener()
		{
			@Override
			public void onClick( View v )
			{				
				
				Network.setNetwork( Network.FACEBOOK, TestActivity.this )
				.sendMessage( Networking.PRIVATE_MESSAGE, message );
			}
		} );
	}

}
