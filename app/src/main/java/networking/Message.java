package networking;

import java.io.Serializable;

import map.work.SportMarker;

public class Message implements Serializable
{

	private static final long serialVersionUID = 1L;
	public final double lat, lon;
	public final String sportType, details;

	public Message(SportMarker marker)
	{
		lat = marker.getLatitude();
		lon = marker.getLongitude();
		sportType = marker.getSport().getSportType().getSportString();
		details = marker.getSport().getDescription();
	}
}
