package networking;

import java.util.ArrayList;
import java.util.Arrays;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;                     
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

////TODO: Deal with this:
import com.example.hakami1024.myapplication.R;

//import com.freesport.sharing_module.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.vk.sdk.api.model.VKApiUserFull;

public class VkFriendsFragment extends ListFragment
{
	protected static final String FRIENDS = "com.freesport.friends";
	private static final String TAG = "VkFriendsFragment";
	private ListView listView;
	
	@Override
	public void onCreate( Bundle savedInstanceState )
	{
		super.onCreate( savedInstanceState );
		
		Log.d(TAG, "onCreate started");
		setHasOptionsMenu(true);
		
		VKApiUserFull[] f;

		try{
			f = (VKApiUserFull[])getArguments().getParcelableArray( FRIENDS );
			Log.d(TAG, "array f created");
			
			ArrayList<VKApiUserFull> friends = new  ArrayList<VKApiUserFull>( Arrays.asList( f ) );
			
			VkListAdapter adapter = new VkListAdapter( friends );
			setListAdapter( adapter );		
		}
		catch( Exception ex)
		{
			Log.d( TAG, "can't create an array f" );
		}
				
	}
	
	@Override
	public View onCreateView( LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState  )
	{
		Log.d(TAG, "onCreateView started");
		View v = inflater.inflate( R.layout.vk_fragment, parent, false );
		
		DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
												.showImageOnLoading( android.R.drawable.stat_notify_sync )
										        .cacheInMemory(true)
										        .cacheOnDisc(true)
										        .build();
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getActivity().getApplicationContext())
																		.defaultDisplayImageOptions(defaultOptions)
																		.build();
		ImageLoader.getInstance().init(config);

		listView = (ListView)v.findViewById( android.R.id.list );	
		
		listView.setChoiceMode( AbsListView.CHOICE_MODE_MULTIPLE );
		
		listView.setOnItemClickListener( new OnItemClickListener()
		{

			@Override
			public void onItemClick( AdapterView<?> parent, View view,
					int position, long id )
			{
				TextView t = (TextView)view.findViewById( R.id.vk_friend_name_tv );
				Log.d(TAG, (String)t.getText()+" position = "+position);
				
				View v = listView.getChildAt(position - 
						listView.getFirstVisiblePosition());
				if( listView.isItemChecked( position ) ){
            		v.setBackgroundColor(0xFFDEE5EB);
                } else {
                	v.setBackgroundColor(0xFFFFFFFF);
                }
				listView.invalidateViews();
				
			}
			
		});
		
		return v;

	}

	private class VkListAdapter extends ArrayAdapter<VKApiUserFull>
	{
		public VkListAdapter( ArrayList<VKApiUserFull> friends )
		{
			super( getActivity(), 0, friends );

		}
		
		@Override
		public View getView( int position, View convertView, ViewGroup parent )
		{
			if( convertView == null )
			{
				convertView = getActivity().getLayoutInflater().inflate( R.layout.vk_friends_list_item, null );
			}
			
			VKApiUserFull friend = getItem( position );
			
			final ListView lv = (ListView) parent;
			final int pos = position;
			convertView.setOnClickListener( new OnClickListener()
			{

				@Override
				public void onClick( View v )
				{
					Log.d("TAG", "onClick listener worked");
					lv.setItemChecked( pos, !lv.isItemChecked( pos ) );
					
				}
			});
            if( lv.isItemChecked( position ) ){
            		convertView.setBackgroundColor(0xFFDEE5EB);
                } else {
                	convertView.setBackgroundColor(0xFFFFFFFF);
                }
			
			TextView nameView = (TextView)convertView.findViewById( R.id.vk_friend_name_tv );
			nameView.setText( friend.first_name + " " + friend.last_name );
			
			ImageView avatar = (ImageView)convertView.findViewById( R.id.vk_avatar );
			ImageLoader.getInstance().displayImage( friend.photo_50, avatar );
			
			return convertView;			
		}
	
	}
	
	@Override
	public void onCreateOptionsMenu( Menu menu, MenuInflater inflater )
	{
		super.onCreateOptionsMenu( menu, inflater );
		Log.d(TAG, "optionsMenu created");
		inflater.inflate( R.menu.vk_menu, menu );
	}
	
	@Override
	public boolean onOptionsItemSelected( MenuItem item )
	{
		switch( item.getItemId() )
		{
			case R.id.vk_friends_send_menu:
				Log.d(TAG, ""+getListView().getCheckedItemCount() );
				String[] userId = new String[ getListView().getCheckedItemCount() ];
				VkListAdapter adapter = (VkListAdapter)getListAdapter();
				
				SparseBooleanArray checked = listView.getCheckedItemPositions();
				for( int i=0, j=0; i<listView.getCount(); i++ )
					if( checked.get( i ) )
					{
						userId[j++] = ""+adapter.getItem( i ).getId();
						Log.d(TAG, adapter.getItem( i ).first_name+" "+adapter.getItem( i ).last_name+" selected" );
					}
				
		        ( (VkNetwork)getActivity()).sendPrivateMessage( userId );
				adapter.notifyDataSetChanged();
				
				return true;
			default:
				return super.onOptionsItemSelected( item );
		}
	}
}