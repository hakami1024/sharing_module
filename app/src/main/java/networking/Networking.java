package networking;

public interface Networking
{

	int WALL = 0;
	int PRIVATE_MESSAGE = 1;
	String MESSAGE_TYPE = "MESSAGE_TYPE";
	String MESSAGE = "MESSAGE";

	void sendMessage( int messageType );
}
