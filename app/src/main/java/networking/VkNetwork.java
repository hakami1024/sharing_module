package networking;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.widget.Toast;

import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCaptchaDialog;
import com.vk.sdk.VKScope;
import com.vk.sdk.VKSdk;
import com.vk.sdk.VKSdkListener;
import com.vk.sdk.VKUIHelper;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKRequest.VKRequestListener;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.api.model.VKApiUserFull;
import com.vk.sdk.api.model.VKUsersArray;
import com.vk.sdk.api.model.VKWallPostResult;
import com.vk.sdk.util.VKUtil;

//TODO: make normal R
import com.example.hakami1024.myapplication.R;

public class VkNetwork extends FragmentActivity implements Networking
{
	private static final String TAG = "VkNetwork";
	private static final String appId = "4237555";

	private static final String[] sMyScopeFull = new String[] { VKScope.WALL,
			VKScope.FRIENDS, VKScope.MESSAGES };
	private static final String[] sMyScopeClient = new String[] { VKScope.WALL, VKScope.FRIENDS  };

	private final Context mContext = this;

	@Override
	public void onCreate( Bundle savedInstanceState )
	{
		super.onCreate( savedInstanceState );
		setContentView( R.layout.vk_activity );

		Log.d( TAG,
				"fingerprint: "
						+ VKUtil.getCertificateFingerprint( this,
								this.getPackageName() )[0] );
		Log.d( TAG, "" + ( this ) + " " + this.getPackageName() );

		final int type = getIntent().getIntExtra( Networking.MESSAGE_TYPE, 0 );
		final boolean forceOAuth = true;// ( type == PRIVATE_MESSAGE );
		final String[] sMyScope = sMyScopeFull;// (type == PRIVATE_MESSAGE ?
												// sMyScopeFull : sMyScopeClient
												// );

		VKUIHelper.onCreate( this );

		VKSdk.initialize( new VKSdkListener()
		{

			@Override
			public void onAccessDenied( VKError authorizationError )
			{
				new AlertDialog.Builder( VkNetwork.this )
						.setMessage(
								"Error connecting to the network. "
										+ authorizationError.errorMessage
										+ "\n(Access Denied error while initialize" )
						.setPositiveButton( "Retry",
								new DialogInterface.OnClickListener()
								{

									@Override
									public void onClick(
											DialogInterface dialog, int which )
									{
										if( VKUtil.isAppInstalled( VkNetwork.this, "com.vkontakte.android" )
												&& VKUtil.isIntentAvailable( VkNetwork.this, "com.vkontakte.android.action.SDK_AUTH") )
										{
											VKSdk.authorize( sMyScopeClient, false, false );
											Toast.makeText( VkNetwork.this, "authorized", Toast.LENGTH_LONG ).show();
										}
										VKSdk.authorize( sMyScope, false, forceOAuth );
									}
								} )
						.setNegativeButton( "Abort all requests",
								new DialogInterface.OnClickListener()
								{
									@Override
									public void onClick(
											DialogInterface dialog, int which )
									{
										Network.proceedSending( Network.ABORT_ALL );
										finish();
									}
								} ).show();
			}

			@Override
			public void onCaptchaError( VKError captchaError )
			{
				new VKCaptchaDialog( captchaError ).show();
			}

			@Override
			public void onTokenExpired( VKAccessToken arg0 )
			{
				Log.d( TAG, "onTokenExpired" );
				if( VKUtil.isAppInstalled( VkNetwork.this, "com.vkontakte.android" )
						&& VKUtil.isIntentAvailable( VkNetwork.this, "com.vkontakte.android.action.SDK_AUTH") )
				{
					VKSdk.authorize( sMyScopeClient, false, false );
					Toast.makeText( VkNetwork.this, "authorized", Toast.LENGTH_LONG ).show();;
				}
				VKSdk.authorize( sMyScope, false, forceOAuth );
				// VKSdk.wakeUpSession();
			}

			@Override
			public void onReceiveNewToken( VKAccessToken newToken )
			{
				sendMessage( type );
			}

		}, appId );

		if( VKSdk.wakeUpSession() )
		{
			sendMessage( type );
		}
		else
		{
			if( VKUtil.isAppInstalled( VkNetwork.this, "com.vkontakte.android" )
					&& VKUtil.isIntentAvailable( VkNetwork.this, "com.vkontakte.android.action.SDK_AUTH") )
			{
				VKSdk.authorize( sMyScopeClient, false, false );
				Toast.makeText( VkNetwork.this, "authorized", Toast.LENGTH_LONG ).show();;
			}
			VKSdk.authorize( sMyScope, false, forceOAuth );
		}

	}

	@Override
	public void onDestroy()
	{
		super.onDestroy();
		VKUIHelper.onDestroy( this );
	}

	@Override
	public void onResume()
	{
		super.onResume();
		VKUIHelper.onResume( this );
	}

	@Override
	public void onActivityResult( int requestCode, int resultCode, Intent data )
	{
		VKUIHelper.onActivityResult( requestCode, resultCode, data );
	}

	@Override
	public void sendMessage( int messageType )
	{
		switch( messageType )
		{
			case WALL:
				postMessage();
				break;
			case PRIVATE_MESSAGE:
				chooseFriend();
				break;
				
		}
		
	}

	private void postMessage()
	{
		final Message message = (Message)getIntent().getSerializableExtra(
				Networking.MESSAGE );

		final VKRequest post = VKApi.wall().post(
				VKParameters.from(
						VKApiConst.OWNER_ID,
						VKSdk.getAccessToken().userId,
						VKApiConst.ATTACHMENTS,
						Network.getMapUrl( message.lat, message.lon, message.details ), VKApiConst.MESSAGE,
						message.details ) );

		post.setModelClass( VKWallPostResult.class );
		proceedMessageSending( post, 0 );

	}

	protected void sendPrivateMessage( String[] friendsId )
	{
		final Message message = (Message)getIntent().getSerializableExtra(
				Networking.MESSAGE );

		FragmentManager fm = getSupportFragmentManager();
		Fragment fragment = fm.findFragmentById( R.id.vk_fragment_container );
		if( fragment != null )
			fm.beginTransaction().remove( fragment ).commit();

		Log.d( TAG, "in sendPrivateMessage. My id: "
				+ VKSdk.getAccessToken().userId );

		for( String friendId : friendsId )
		{
			Log.d( TAG, "sending to: " + friendId );
			final VKRequest sending = new VKRequest( "messages.send",
					VKParameters.from(
							VKApiConst.USER_ID,
							friendId,
							VKApiConst.MESSAGE,
							message.details
									+ "\n"
									+ Network.getMapUrl( message.lat, message.lon,
											message.details ) ) );

			proceedMessageSending( sending, 0 );
		}

	}

	@SuppressWarnings("serial")
	private void proceedMessageSending( final VKRequest sending, final int times )
	{
		if( times == 0 )
		{
			sending.executeWithListener( new VKRequestListener()
			{

				@Override
				public void onComplete( VKResponse response )
				{
					super.onComplete( response );
					Network.proceedSending( Network.FINISHED );
					finish();
				}

				@Override
				public void onError( VKError error )
				{
					Log.d( TAG, "onError!" );

					if( !VKSdk.isLoggedIn()
							|| VKSdk.getAccessToken().isExpired() )
						Toast.makeText( mContext, "Таки дело в токене.",
								Toast.LENGTH_LONG ).show();

					if( times < 10 )
					{
						proceedMessageSending( sending, times + 1 );
					}
					else
					{
						new AlertDialog.Builder( VkNetwork.this )
								.setMessage(
										"Message sending failed. "
												+ error.errorMessage + " "
												+ error.errorCode )
								.setPositiveButton( "Abort all",
										new DialogInterface.OnClickListener()
										{
											@Override
											public void onClick(
													DialogInterface dialog,
													int which )
											{
												Network.proceedSending( Network.ABORT_ALL );
												finish();
											}
										} )
								.setNeutralButton( "Cancel this sending", 
										new DialogInterface.OnClickListener()
										{
											@Override
											public void onClick(
													DialogInterface dialog, int which )
											{
												Network.proceedSending( Network.FINISHED );
												finish();
											}
										} )
								.setNegativeButton( "Retry",
										new DialogInterface.OnClickListener()
										{
											@Override
											public void onClick(
													DialogInterface dialog,
													int which )
											{
												proceedMessageSending( sending, 0 );
											}
										} ).show();
					}
				}

			} );
		}
		else
			sending.repeat();

	}

	private void chooseFriend()
	{
		final VKRequest friendsReq = VKApi.friends().get(
				VKParameters.from( VKApiConst.USER_ID,
						VKSdk.getAccessToken().userId, "order", "hints",
						VKApiConst.FIELDS, "photo_50" ) );
		friendsReq.setModelClass( VKUsersArray.class );
		friendsReq.executeWithListener( new VKRequestListener()
		{

			private static final long serialVersionUID = 5787159317302570550L;

			@Override
			public void onComplete( VKResponse response )
			{
				Log.d( TAG, "in onComplete, chooseFriends" );

				VKUsersArray friends = (VKUsersArray)response.parsedModel;

				FragmentManager fm = getSupportFragmentManager();
				Fragment fragment = fm
						.findFragmentById( R.id.vk_fragment_container );

				if( fragment == null )
				{
					Log.d( TAG, "fragment == null, chooseFriends" );

					fragment = new VkFriendsFragment();
					Bundle args = new Bundle();
					VKApiUserFull[] f = new VKApiUserFull[friends.size()];
					friends.toArray( f );
					args.putParcelableArray( VkFriendsFragment.FRIENDS, f );
					fragment.setArguments( args );
					fm.beginTransaction()
							.add( R.id.vk_fragment_container, fragment )
							.commit();
				}

				// VKApiUserFull friend = friends.getById( 65554412 );
				// Log.d( TAG, "Daniyar's last_name: " + friend.last_name );
			}

			@Override
			public void onError( VKError error )
			{
				Log.d( TAG, "onError!" );

				if( VKSdk.getAccessToken().isExpired() )
				{
					Log.d( TAG, "token expired. repeat..." );

					Editor editor = mContext.getSharedPreferences(
							"clear_cache", Context.MODE_PRIVATE ).edit();
					editor.clear();
					editor.commit();

					VKSdk.authorize( sMyScopeFull, false, true );
					// friendsReq.repeat();

				}

				new AlertDialog.Builder( VkNetwork.this )
						.setMessage(
								"Can't get friends list. " + error.errorMessage
										+ " " + error.toString() )
						.setPositiveButton( "Cancel",
								new DialogInterface.OnClickListener()
								{

									@Override
									public void onClick(
											DialogInterface dialog, int which )
									{
										Network.proceedSending( Network.FINISHED );
										finish();
									}
								} )
						.setNegativeButton( "Retry",
								new DialogInterface.OnClickListener()
								{
									@Override
									public void onClick(
											DialogInterface dialog, int which )
									{
										friendsReq.repeat();
									}
								} ).show();
			}
		} );
	}

}
