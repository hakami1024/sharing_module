package networking;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import com.facebook.widget.FacebookDialog;

public class FbMessaging extends Fragment
{
	@SuppressWarnings("unused")
	private static final String TAG = "FbMessaging";
	
	@Override
	public void onCreate( Bundle savedInstanceState)
	{
		super.onCreate( savedInstanceState );
		Message message = (Message)getActivity().getIntent().getSerializableExtra( Networking.MESSAGE  );
		
		FacebookDialog.MessageDialogBuilder builder = new FacebookDialog.MessageDialogBuilder((Activity)getActivity() )
        .setLink(Network.getMapUrl( message.lat, message.lon, message.details ))
        .setName("Назначена встреча. "+message.sportType )
        .setCaption(message.details)
        //.setPicture("http://i.imgur.com/g3Qc1HN.png")
        .setDescription(message.details)
        .setFragment( this );
        
        // If the Facebook app is installed and we can present the share dialog
        if (builder.canPresent()) {
        	FacebookDialog dialog = builder.build();
        	dialog.present();
    		}  
        else {
          Toast.makeText( getActivity(), "Facebook Messenger requered", Toast.LENGTH_LONG ).show();
        }
    	
	}
}
