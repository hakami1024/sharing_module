package networking;

import java.util.PriorityQueue;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class Network 
{
	private static final String TAG = "Network";

	public static final int VK = 0;
	public static final int FACEBOOK = 1;
	public static final int TWITTER = 2;
	
	public static final int WALL = 10;
	public static final int PRIVATE_MESSAGE = 11;
	protected static final int USER_INFO = 12;
	
	//protected static final int CANT_CONNECT = 101;
	protected static final int FINISHED = 102;
	protected static final int ABORT_ALL = 104;
	
	private static int curNetwork = VK;
	private static Context mContext;
	
	@SuppressWarnings("rawtypes")
	private static final Class []classes = { VkNetwork.class, FbNetwork.class, null };
	
	private static PriorityQueue<Intent> requests = new PriorityQueue<Intent>();
	private static boolean isActive = false;

	private static Network network = new Network();

	public static Network setNetwork(final int name, Context context)
	{
		curNetwork = name;
		mContext = context;
		return network;
	}
	
	private Network(){} 
	
	public static Network getCurrentNetwork()
	{
		return network;
	}

	public void sendMessage( int messageType, Message message )
	{
		Intent i = new Intent( mContext, classes[ curNetwork ] );
		i.putExtra( Networking.MESSAGE_TYPE, messageType );
		i.putExtra( Networking.MESSAGE, message );
		requests.add( i );
		
		proceedSending( Network.FINISHED );
		Log.d(TAG, "in sendMessage, after activity starting");

	}
	
	public static Context getContext()
	{
		return mContext;
	}

	protected static void proceedSending( int result )
	{
		if( result == ABORT_ALL )
			requests.clear();
		else if( !isActive && !requests.isEmpty() && result == FINISHED )
		{
			isActive = true;
			Log.d(TAG, "requests: "+requests.size());
			mContext.startActivity( requests.poll() );
		}
		
		if( requests.isEmpty() )
			isActive = false;
	}
	
	public UserInfo InitUser()
	{
		UserInfo ui = network.new UserInfo();
		return ui;
	}
	
	public class UserInfo
	{
		private String userName = null;
		private String userId = null;
		
		private UserInfo(){}
		
		protected void setUserName( String name )
		{
			userName = name;
		}
		
		protected void setUserId( String id )
		{
			userId = id;
		}
		
		public String getUserName()
		{
			return userName;
		}
		
		public String getUserId()
		{
			return userId;
		}
	}
	
	protected static String getMapUrl( double lat, double lon, String label )
	{
		try
		{
			label = java.net.URLEncoder.encode( label, "UTF-8" );
		}
		catch (Exception e){}
		return "https://maps.google.com/maps?q=" + lat + "%20" + lon + "%28%22"
				+ label + "%22%29";
	}

}
