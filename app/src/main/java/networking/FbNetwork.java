package networking;

import java.util.List;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.widget.Toast;

import com.facebook.FacebookException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.FacebookDialog;
import com.facebook.widget.FriendPickerFragment;
import com.facebook.widget.PickerFragment;
import com.facebook.widget.WebDialog;
import com.facebook.widget.WebDialog.OnCompleteListener;
import com.example.hakami1024.myapplication.R;
//import com.freesport.sharing_module.R;

public class FbNetwork extends FragmentActivity implements Networking{
	
	protected UiLifecycleHelper uiHelper;
	
	private static final String TAG = "FbNetwork";
	FriendPickerFragment friendPickerFragment;
	@Override
	protected void onCreate( Bundle savedInstanceState )
	{
		super.onCreate(savedInstanceState);
		uiHelper = new UiLifecycleHelper(this, null);
	    uiHelper.onCreate(savedInstanceState);
	    setContentView( R.layout.fb_activity );
		
	  // start Facebook Login
		
	  Session.openActiveSession(this, true, new Session.StatusCallback() {

	    // callback when session changes state
		@Override
		public void call( Session session, SessionState state, Exception exception )
		{
			if(session.isOpened()) 
			{
				Log.d(TAG, "Session is opened - logined");
				sendMessage( 0 );
		    }
		}
	  });
		

	}
	
	@Override
	protected void onStart() {
	    super.onStart();
	    if ( Uri.parse("picker://friend").equals(getIntent().getData())) {
	        try {
	            friendPickerFragment.loadData(false);
	        } catch (Exception ex) {
	            onError(ex);
	        }
	    }
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
	  super.onActivityResult(requestCode, resultCode, data);
	  Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
	  Log.e(TAG, "Success is near");
	  uiHelper.onActivityResult(requestCode, resultCode, data, new FacebookDialog.Callback() {
	        @Override
	        public void onError(FacebookDialog.PendingCall pendingCall, Exception error, Bundle data) {
	            Log.e(TAG, String.format("Error: %s", error.toString()));
	        }

	        @Override
	        public void onComplete(FacebookDialog.PendingCall pendingCall, Bundle data) {
	            Log.e(TAG, "Success!");
	            Network.proceedSending( Network.FINISHED );
	            finish();
	        }
	    });
	  Network.proceedSending( Network.FINISHED );
      finish();
	}
	
	@Override
	protected void onResume() {
	    super.onResume();
	    uiHelper.onResume();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
	    super.onSaveInstanceState(outState);
	    uiHelper.onSaveInstanceState(outState);
	}

	@Override
	public void onPause() {
	    super.onPause();
	    uiHelper.onPause();
	}

	@Override
	public void onDestroy() {
	    super.onDestroy();
	    uiHelper.onDestroy();
	}

	@Override
	public void sendMessage( int messageType )
	{
		final int type = getIntent().getIntExtra( Networking.MESSAGE_TYPE, 0 );
		Log.d(TAG, "in sendMessage, type = "+type);

		switch( type )
		{
			case Networking.WALL:
				postMessage();
				break;
			case Networking.PRIVATE_MESSAGE:
				sendPrivateMessage();
				break;
		}
	}

	private void postMessage()
	{
		Log.d(TAG, "posting to a wall");
		final Message message = (Message)getIntent().getSerializableExtra( Networking.MESSAGE );

		if( FacebookDialog.canPresentShareDialog(getApplicationContext(), 
                FacebookDialog.ShareDialogFeature.SHARE_DIALOG) )
		{
			FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder( FbNetwork.this )
			.setDescription( message.details )
	        .setLink( Network.getMapUrl( message.lat, message.lon, message.details ) )
	        .build();
			uiHelper.trackPendingDialogCall(shareDialog.present());
		}
		else {
			proceedSendingMessage("");
		}
	}

	private void sendPrivateMessage()
	{
		if( FacebookDialog.canPresentMessageDialog( FbNetwork.this, FacebookDialog.MessageDialogFeature.MESSAGE_DIALOG ) )
    	{
			FragmentManager manager = getSupportFragmentManager();
			Fragment fragmentToShow = new FbMessaging();
	        manager.beginTransaction().add( R.id.picker_fragment, fragmentToShow ).commit();
    	}
    	else
    	{
    		FragmentManager manager = getSupportFragmentManager();
		    Fragment fragmentToShow = null;
		    Bundle args = getIntent().setData(Uri.parse("picker://friend")).getExtras(); 
		    
		    friendPickerFragment = manager.findFragmentById( R.id.picker_fragment ) == null ?
		    		new FriendPickerFragment( args ) : (FriendPickerFragment) manager.findFragmentById( R.id.picker_fragment);
	        
	        // Set the listener to handle errors
	        friendPickerFragment.setOnErrorListener(new PickerFragment.OnErrorListener() {
	            @Override
	            public void onError(PickerFragment<?> fragment,
	                                FacebookException error) {
	            	FbNetwork.this.onError( error.getLocalizedMessage(), false);
	            }
	        });
	        // Set the listener to handle button clicks
	        friendPickerFragment.setOnDoneButtonClickedListener( 
	        	new PickerFragment.OnDoneButtonClickedListener() 
	                {
			            @Override
			            public void onDoneButtonClicked(PickerFragment<?> fragment) 
			            {
			            	List<GraphUser> users = friendPickerFragment.getSelection();
			            	FragmentManager fm =getSupportFragmentManager();
		            		fm.beginTransaction().remove( friendPickerFragment ).commit();
			            	String toIDs = "";
			            	if( users.isEmpty() )
			            	{
			            		Network.proceedSending( Network.FINISHED );
			            		finish();
			            	}
			            	else
			            	{
				            	for( GraphUser user: users )
				            	{
				            		toIDs += user.getId() + ",";
				            	}
				            	proceedSendingMessage( toIDs );
			            	}
			            }
	                }
	        );
	        fragmentToShow = friendPickerFragment;
		    
		    manager.beginTransaction()
		           .replace(R.id.picker_fragment, fragmentToShow)
		           .commit();
    	}
		 
	}
	
	private void onError(Exception error) {
	    onError(error.getLocalizedMessage(), false);
	}

	private void onError(String error, final boolean finishActivity) {
	    AlertDialog.Builder builder = new AlertDialog.Builder(this);
	    builder.setTitle(R.string.error_dialog_title).
	            setMessage(error).
	            setPositiveButton(R.string.error_dialog_button_text, 
	               new DialogInterface.OnClickListener() {
	                @Override
	                public void onClick(DialogInterface dialogInterface, int i) {
	                    if (finishActivity) {
	                    	finish();
	                    }
	                }
	            });
	    builder.show();
	}

	private void proceedSendingMessage( final String toIDs )
	{
		final Message message = (Message)getIntent().getSerializableExtra( Networking.MESSAGE );
		Bundle params = new Bundle();
		params.putString("name", "Freesport: what about meeting?");
	    params.putString("description", message.details);
	    params.putString("link", Network.getMapUrl( message.lat, message.lon, message.details ) );
        if( toIDs != "" ) params.putString( "to", toIDs );
            
        WebDialog feedDialog = (
            new WebDialog.FeedDialogBuilder(FbNetwork.this,
                Session.getActiveSession(),
                params))
            .setOnCompleteListener(new OnCompleteListener() {
                @Override
                public void onComplete(Bundle values,
                    FacebookException error) {
                    if (error == null) {
                        // When the story is posted, echo the success
                        // and the post Id.
                        final String postId = values.getString("post_id");
                        if (postId != null) {
                            Toast.makeText(FbNetwork.this,
                                "Сообщение опубликовано",
                                Toast.LENGTH_SHORT).show();
                        } 
                    } else if (!(error instanceof FacebookOperationCanceledException) ){
                        // Generic, ex: network error
                    	new AlertDialog.Builder( FbNetwork.this )
						.setMessage(
								"Error connecting to the network. Message wasn't sended." )
						.setPositiveButton( "Retry",
								new DialogInterface.OnClickListener()
								{
									@Override
									public void onClick(
											DialogInterface dialog, int which )
									{
										proceedSendingMessage( toIDs );
									}
								} )
						.setNeutralButton( "Cancel sending", 
								new DialogInterface.OnClickListener()
								{
									
									@Override
									public void onClick( DialogInterface dialog, int which )
									{
										Network.proceedSending( Network.FINISHED );
										finish();
									}
								})
						.setNegativeButton( "Abort all requests",
								new DialogInterface.OnClickListener()
								{
									@Override
									public void onClick(
											DialogInterface dialog, int which )
									{
										Network.proceedSending( Network.ABORT_ALL );
										finish();
									}
								} ).show();
                    }
                    Network.proceedSending( Network.FINISHED );
            		finish();
                }

            })
            .build();
        feedDialog.show();
	}
}