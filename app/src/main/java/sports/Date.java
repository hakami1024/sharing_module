package sports;

public class Date
{

	private String date;

	public String getDate()
	{
		return date;
	}

	public void setDate( String date )
	{
		this.date = date;
	}

	public Date(String date)
	{
		setDate( date );
	}

	@Override
	public String toString()
	{
		return getDate();
	}

}
