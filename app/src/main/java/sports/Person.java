package sports;

public class Person
{

	private String login;

	private String password;

	public Person(String login, String password)
	{
		setLogin( login );
		setPassword( password );
	}

	public String getLogin()
	{
		return login;
	}

	public void setLogin( String login )
	{
		this.login = login;
	}

	public String getPassword()
	{
		return password;
	}

	public void setPassword( String password )
	{
		this.password = password;
	}

	public boolean equals( Person p )
	{
		return this.getLogin().equals( p.getLogin() )
				&& this.getPassword().equals( p.getPassword() );
	}

}
