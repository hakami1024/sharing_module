package sports;

public class Sport
{

	private SportTypes sportType;

	private Time time;
	private Date date;

	private String description;

	public Sport(SportTypes sportType, String time, String date,
			String description)
	{
		setSportType( sportType );
		setTime( new Time( time ) );
		setDate( new Date( date ) );
		setDescription( description );
	}

	public SportTypes getSportType()
	{
		return sportType;
	}

	public void setSportType( SportTypes sportType )
	{
		this.sportType = sportType;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription( String description )
	{
		this.description = description;
	}

	public String sportToString()
	{
		return "\tSport type: " + getSportType().getSportString()
				+ "\n\tDate: " + getDate().toString() + "\n\tTime: "
				+ getTime().toString() + "\n\tAdditional Description: "
				+ getDescription();
	}

	public Time getTime()
	{
		return time;
	}

	public void setTime( Time time )
	{
		this.time = time;
	}

	public Date getDate()
	{
		return date;
	}

	public void setDate( Date date )
	{
		this.date = date;
	}

}
