package sports;

import com.google.android.gms.maps.model.BitmapDescriptor;

public enum SportTypes
{

	Basketball("Basketball", null), Fitness("Fitness", null), Football(
			"Football", null), Streetball("Streetball", null), Tennis("Tennis",
			null), TrackAndField("Track and field", null), Other("Other", null);

	private final String sportString;

	private final BitmapDescriptor markerIcon;

	public String getSportString()
	{
		return sportString;
	}

	public BitmapDescriptor getMarkerIcon()
	{
		return markerIcon;
	}

	private SportTypes(String sportString, BitmapDescriptor icon)
	{
		this.sportString = sportString;
		this.markerIcon = icon;
	}

	public static SportTypes valueOfString( String sport )
	{
		// so bad, that android needs jdk 1.6, we can't use switch for strings
		if( sport.equals( "Basketball" ) )
			return SportTypes.Basketball;
		if( sport.equals( "Fitness" ) )
			return SportTypes.Fitness;
		if( sport.equals( "Football" ) )
			return SportTypes.Football;
		if( sport.equals( "Streetball" ) )
			return SportTypes.Streetball;
		if( sport.equals( "Tennis" ) )
			return SportTypes.Tennis;
		if( sport.equals( "Track and field" ) )
			return SportTypes.TrackAndField;
		if( sport.equals( "Other" ) )
			return SportTypes.Other;
		return null;
	}
}
