package sports;

public class Time
{

	private String time;

	public String getTime()
	{
		return time;
	}

	public void setTime( String time )
	{
		this.time = time;
	}

	public Time(String time)
	{
		setTime( time );
	}

	@Override
	public String toString()
	{
		return getTime();
	}
}
